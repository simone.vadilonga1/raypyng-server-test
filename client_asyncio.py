import asyncio
import os
import csv

class ClientProtocol(asyncio.Protocol):
    def __init__(self, exports_list, file_path):
        self.exports_list = exports_list
        self.file_path = file_path
        self.filename = None
        self.received_data = ''
        self.finished = asyncio.Future()  # Future to signal when the connection is closed

    def connection_made(self, transport):
        print('Connection made, about to send data')
        self.transport = transport
        self.loop = asyncio.get_event_loop()

        with open(os.path.join(self.file_path, 'beamline.rml'), 'rb') as file:
            file_content = file.read()

        data_to_send = f"{','.join(self.exports_list)}|||{file_content.decode('utf-8')}"
        self.transport.write(data_to_send.encode('utf-8'))
        print('Sent data')

        files = os.listdir(self.file_path)
        for file in files:
            if file.endswith(".csv"):
                file_path = os.path.join(self.file_path, file)
                os.remove(file_path)
                print(f"Removed: {file}")

    def data_received(self, data):
        if "|ENDOFTRANSMISSION|" in data.decode('utf-8'):
            print('Received all the data')
            self.received_data += (data.decode('utf-8'))
            self.received_data = self.received_data.replace("|ENDOFTRANSMISSION|", "")
            self.received_data = self.received_data[0:-3]
            
            # # Save received data to a file for debugging
            # debug_file_path = os.path.join(self.file_path, 'data-received.txt')
            # with open(debug_file_path, 'a') as debug_file:
            #     debug_file.write(self.received_data)

            received_transmission_list = self.received_data.split('|||')
            for i in range(0, len(received_transmission_list), 2):
                filename = received_transmission_list[i]
                file_content = received_transmission_list[i + 1]
                if filename == self.filename:
                    open_file_mode = 'a'
                else:
                    open_file_mode = 'w'
                save_path = os.path.join(self.file_path, filename)
                with open(save_path, open_file_mode, newline='') as csvfile:
                    csvwriter = csv.writer(csvfile)
                    data_lines = file_content.splitlines()
                    for line in data_lines:
                        csvwriter.writerow(line.split(','))
                self.filename = filename
            self.finished.set_result(True)  # Signal completion when data is processed
        else:
            self.received_data += (data.decode('utf-8'))

    def connection_lost(self, exc):
        print("Connection lost:", exc)
        self.finished.set_result(True)  # Signal completion when the connection is lost

async def run_main_protocol(transport, protocol):
    try:
        # Your existing code for handling the main protocol
        await protocol.finished

    except KeyboardInterrupt:
        pass
    finally:
        pass # Close the transport to ensure the connection is properly closed
        # transport.close()
        # await transport.wait_closed()

async def main():
    exports_list = ["KB2", "DetectorAtFocus", "KB1", "Dipole"]
    file_path = 'tmp_client'

    loop = asyncio.get_event_loop()
    transport, protocol = await loop.create_connection(lambda: ClientProtocol(exports_list, file_path), 'localhost', 12345)

    # Create tasks for the main coroutine and waiting for protocol.finished
    main_task = asyncio.create_task(run_main_protocol(transport, protocol))

    # Wait for both tasks to complete
    await asyncio.gather(main_task, protocol.finished)

if __name__ == "__main__":
    asyncio.run(main())



