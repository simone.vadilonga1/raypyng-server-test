import asyncio
import os
import csv
from simulate import do_simulation

class ServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        print('connection')
        self.transport = transport

    def data_received(self, data):
        print('data received')
        # Process received data (exports list and file content)
        data_str = data.decode('utf-8')
        exports_list, file_content = data_str.split('|||', 1)

        with open('tmp_server/beamline.rml', 'w') as file:
            file.write(file_content)

        exports_list = exports_list.split(',')
        print(f'exports_list {exports_list}')

        # Perform simulation with the received exports list
        sim_done = do_simulation(exports_list, rml_file_path='tmp_server/beamline.rml')

        # Send files in chunks for files larger than the chunk size
        chunk_size = 4096
        for obj in exports_list:
            result_filename = f'{obj}-RawRaysOutgoing.csv'
            result_path = os.path.join('tmp_server', result_filename)

            with open(result_path, 'rb') as file:
                remaining_data = file.read()
                last_complete_row = ''

                while remaining_data:
                    chunk = remaining_data[:chunk_size]
                    remaining_data = remaining_data[chunk_size:]

                    # Ensure the chunk ends with a complete row
                    while not chunk.endswith(b'\n') and remaining_data:
                        next_byte = remaining_data[0:1]
                        remaining_data = remaining_data[1:]
                        chunk += next_byte

                    self.transport.write(f"{result_filename}|||{chunk.decode('utf-8')}|||".encode('utf-8'))


        # Send the |ENDOFTRANSMISSION| after sending all chunks
        self.transport.write(f"|ENDOFTRANSMISSION|".encode('utf-8'))

    def connection_lost(self, exc):
        print("Connection lost:", exc)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    server_coro = loop.create_server(ServerProtocol, '127.0.0.1', 12345)
    server = loop.run_until_complete(server_coro)
    print("Server listening on", server.sockets[0].getsockname())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()
