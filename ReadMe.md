# RAYPYNG Simulation Project

## Overview

The RAYPYNG Simulation Project is a client-server application for performing ray tracing simulations. The project consists of a server that performs simulations based on user-defined parameters and a client that sends simulation input to the server and receives the simulation results.

## Features

- **Client-Server Communication**: The client sends a list of simulation objects and an XML file to the server for processing.
- **Simulation Processing**: The server performs simulations based on the received data and generates result files.
- **File Transfer**: The client and server exchange files, including the simulation input and result files.

## Project Structure

- `client.py`: The updated client-side script responsible for sending simulation data to the server.
- `server.py`: The updated server-side script that receives simulation data, performs simulations, and sends back result files.
- `simulate.py`: Module containing simulation logic using RayPyNG.
- `tmp_client/`: Directory for storing temporary simulation input files on the client side.
- `tmp_server/`: Directory for storing temporary simulation and result files on the server side.

## How to Run

1. Ensure you have Python installed on your machine.

2. Install project dependencies:

    ```bash
    pip install -r requirements.txt
    ```

   Make sure you follow the instructions for [RayPyNG](https://raypyng.readthedocs.io/en/latest/).

3. Run the server:

    ```bash
    python server.py
    ```

4. In a separate terminal, run the client:

    ```bash
    python client.py
    ```

   Follow the prompts to provide simulation input.

## Configuration

- Customize the list of simulation objects in the `export_list` variable in `client.py`.

## Dependencies

- Python 3.x, RayPyNG, Twisted

## License

This project is licensed under the [MIT License](LICENSE).
