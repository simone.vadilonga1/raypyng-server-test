from twisted.internet import reactor, protocol
import os
import csv

class ClientProtocol(protocol.Protocol):
    """
    Protocol for handling client-side communication with the server.

    Attributes:
        exports_list (list): List of simulation objects.
        file_path (str): Path to the directory for storing received files.
        filename (str): Name of the currently processed file.
        received_data (str): Accumulated data received from the server.
    """

    def __init__(self, exports_list, file_path):
        """
        Initializes the ClientProtocol.

        Args:
            exports_list (list): List of simulation objects.
            file_path (str): Path to the directory for storing received files.
        """
        self.exports_list = exports_list
        self.file_path = file_path
        self.filename = None
        self.received_data = ''

    def connectionMade(self):
        """
        Triggered when the connection is established. Sends simulation data to the server.
        """
        with open(os.path.join(self.file_path, 'beamline.rml'), 'rb') as file:
            file_content = file.read()

        data_to_send = f"{','.join(self.exports_list)}|||{file_content.decode('utf-8')}"
        self.transport.write(data_to_send.encode('utf-8'))

        # remove all the csv files from the tmp folder
        # Get a list of all files in the folder
        files = os.listdir(self.file_path)

        # Iterate through the files and remove .csv files
        for file in files:
            if file.endswith(".csv"):
                file_path = os.path.join(self.file_path, file)
                os.remove(file_path)
                print(f"Removed: {file}")

    def dataReceived(self, data):
        """
        Triggered when data is received from the server. Handles the received data.

        Args:
            data (bytes): Received data.
        """
        if "|ENDOFTRANSMISSION|" in data.decode('utf-8'):
            print('I received all the data')
            received_transmission_list = self.received_data.split('|||')
            for i in range(0, len(received_transmission_list), 2):
                filename = received_transmission_list[i]
                file_content = received_transmission_list[i + 1]
                if filename == self.filename:
                    open_file_mode = 'a'
                else:
                    open_file_mode = 'w'
                save_path = os.path.join(self.file_path, filename)
                with open(save_path, open_file_mode, newline='') as csvfile:
                    csvwriter = csv.writer(csvfile)
                    data_lines = file_content.splitlines()
                    for line in data_lines:
                        csvwriter.writerow(line.split(','))
                self.filename = filename
        else:
            self.received_data += (data.decode('utf-8'))

    def connectionLost(self, reason):
        """
        Triggered when the connection is lost. Stops the reactor.

        Args:
            reason (twisted.python.failure.Failure): The reason for the connection loss.
        """
        print("Connection lost:", reason)
        reactor.stop()

class ClientFactory(protocol.ClientFactory):
    """
    Factory for creating instances of ClientProtocol.

    Attributes:
        exports_list (list): List of simulation objects.
        file_path (str): Path to the directory for storing received files.
    """

    def __init__(self, exports_list, file_path):
        """
        Initializes the ClientFactory.

        Args:
            exports_list (list): List of simulation objects.
            file_path (str): Path to the directory for storing received files.
        """
        self.exports_list = exports_list
        self.file_path = file_path

    def buildProtocol(self, addr):
        """
        Builds and returns an instance of ClientProtocol.

        Args:
            addr (twisted.internet.address.HostnameAddress): The address of the server.

        Returns:
            ClientProtocol: An instance of ClientProtocol.
        """
        return ClientProtocol(self.exports_list, self.file_path)

    def clientConnectionFailed(self, connector, reason):
        """
        Triggered when the client connection fails. Stops the reactor.

        Args:
            connector (twisted.internet.tcp.Connector): The client connector.
            reason (twisted.python.failure.Failure): The reason for the connection failure.
        """
        print(f"Connection failed: {reason}")
        reactor.stop()

if __name__ == "__main__":
    exports_list = ["KB2", "DetectorAtFocus", "KB1", "Dipole"]
    file_path = 'tmp_client'

    reactor.connectTCP('localhost', 12345, ClientFactory(exports_list, file_path))
    reactor.run()
