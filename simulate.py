from raypyng.runner import RayUIRunner, RayUIAPI
import os


def do_simulation(export_list, ray_path=None, rml_file_path='tmp/beamline_received.rml', verbose=False):
    r = RayUIRunner(ray_path=ray_path, hide=True)
    a = RayUIAPI(r)
    r.run()
    a.load(rml_file_path)
    if verbose:
        print('file loaded')
    a.trace(a.trace(analyze=True))
    if verbose:
        print('File traced')
    current_script_path = os.path.dirname(os.path.realpath(__file__))
    tmp_folder = os.path.join(current_script_path, 'tmp_server')
    if verbose:
        print(type(tmp_folder), tmp_folder)
    for obj in export_list:
        a.export(obj, "RawRaysOutgoing", tmp_folder, '')
    a.quit()
    return True

