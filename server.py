from twisted.internet import protocol, reactor
import os
from simulate import do_simulation

class ServerProtocol(protocol.Protocol):
    """
    Protocol for handling communication with the client on the server side.
    """

    def connectionMade(self):
        """
        Called when a connection is made. Prints information about the connected peer.
        """
        print("Connection from", self.transport.getPeer())

    def dataReceived(self, data):
        """
        Called when data is received from the client. Processes the received data and performs simulations.

        Args:
            data (bytes): The received data containing exports list and file content.
        """
        # Process received data (exports list and file content)
        data_str = data.decode('utf-8')
        exports_list, file_content = data_str.split('|||', 1)

        with open('tmp_server/beamline.rml', 'w') as file:
            # Write the string to the file
            file.write(file_content)

        exports_list = exports_list.split(',')
        print(f'exports_list {exports_list}')

        # Perform simulation with the received exports list
        sim_done = do_simulation(exports_list, rml_file_path='tmp_server/beamline.rml')

        # Send files in chunks for files larger than the chunk size
        chunk_size = 4096  # You can adjust this value based on your needs
        for obj in exports_list:
            result_filename = f'{obj}-RawRaysOutgoing.csv'
            result_path = os.path.join('tmp_server', result_filename)
            
            file_size = os.path.getsize(result_path)

            if file_size > chunk_size:
                with open(result_path, 'rb') as file:
                    remaining_data = file.read()
                    while remaining_data:
                        # Find the index of the next newline character within the chunk_size limit
                        newline_index = remaining_data.rfind(b'\n', 0, chunk_size)
                        
                        if newline_index == -1:
                            # No newline found in the chunk, send the entire remaining_data
                            chunk = remaining_data
                            remaining_data = b''
                        else:
                            # Send up to the last complete line in the chunk
                            chunk = remaining_data[:newline_index + 1]
                            remaining_data = remaining_data[newline_index + 1:]

                        self.transport.write(f"{result_filename}|||{chunk.decode('utf-8')}|||".encode('utf-8'))
            else:
                # If the file is smaller than the chunk size, send it as a whole
                with open(result_path, 'rb') as file:
                    file_content = file.read()
                    self.transport.write(f"{result_filename}|||{file_content.decode('utf-8')}|||".encode('utf-8'))

        self.transport.write(f"|ENDOFTRANSMISSION|".encode('utf-8'))
        self.transport.loseConnection()

class ServerFactory(protocol.Factory):
    """
    Factory class for creating ServerProtocol instances.
    """
    def buildProtocol(self, addr):
        """
        Called when a connection is made. Returns an instance of ServerProtocol.

        Args:
            addr (tuple): A tuple representing the address of the connecting peer.

        Returns:
            ServerProtocol: An instance of the ServerProtocol.
        """
        return ServerProtocol()

if __name__ == "__main__":
    reactor.listenTCP(12345, ServerFactory())
    print("Server listening on port 12345")
    reactor.run()
